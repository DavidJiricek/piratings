Piratings
=========

**Piratings** are performance ratings for sports teams. 
They were originally intended for measuring performance of football teams, 
but they are useful for other sports as well (ice-hockey, basketball, handball...).

Piratings enable user to predict outcome of match in the terms of the score difference.

Piratings have several notable features making them 
superior to other ranking systems (such as ELO). They:
- capture home-court advantage;
	each team has _home rating_ and _away rating_, related to its home or away matches
- understand that win is more important than high score difference
	high error between predicted and actual score difference 
	is exponentially diminished prior updating piratings
- give higher weight to the recent matches 
	piratings feature adjustable learning rates _lambda_ and _gamma_

The pirating system was developed by [Constantinou and Fenton (2013)](https://doi.org/10.1515/jqas-2012-0036).

Implementation and usage
========================

Implementation in Python contains two classes: _PiRatingsModel_ and _Optimization_.

_PiRatingModel_ creates and stores piratings of teams based on given matches.
Piratings are stored in dictionary. This class can predict score difference based on piratings.

Each match must be described using (at least) 4 numbers: 

- Home Team ID | Away Team ID | Home Team Score | Away Team Score

Ordered list (from oldest to newest) of **n** matches is then a matrix (**n** x **4**).
PiRatingModel process such a matrix using method _process\_matches_, 
which expects **columns** of this "match matrix" as arguments.

_Order_ of rows in "match matrix" is important, because piratings are update sequentially;
order represents how the matches were played in time. 

This is the main difference from "feature matrices" in machine learning,
where the order of rows (observations) is not important.

_Optimization_ is helper class for performing grid search to find 
the best values of PiRatingModel hyperparameters = _lambda_, _gamma_, _c_.





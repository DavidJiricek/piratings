import numpy as np


class PiRatingsModel:
    """
    Create model for computing and storing piratings - performance ratings for sports teams.
    Each team has "home rating" and "away rating", related to its home or away matches.

    Ratings can be used to predict score difference between two teams (=result) of the future match.

    After a match is played, ratings are adjusted accordingly to the error of the prediction.
    Error is difference between predicted score and actual score.

    Parameters
        lamb : float
            learning rate for rating updates, describing how fast rating reacts on recent performance
        gamma : float
            additional learning rate, describing how fast "home rating" reacts on away performance and vice-versa
        c : float
            constant, default value 3

    Attributes
        teams__h_rating : dict
            "home rating" of every team
        teams__a_rating : dict
            "away rating" of every team
        matches__h_rating : list
            for each processed match store "home rating" of the home team at the time of the match
        matches__a_rating : list
            for each processed match store "away rating" of the away team at the time of the match
        matches__error : list
            squared error between predicted score difference and actual score difference
        match_processed_count : int
            counts the number of processed match
        MSE : float
            mean squared error from all processed matches
    """
    def __init__(self, c=3, gamma=0.7, lamb=0.035, store_history=False):
        self.c = c
        self.gamma = gamma
        self.lamb = lamb
        self.store_history = store_history

        self.teams__h_rating = {}
        self.teams__a_rating = {}

        self.matches__h_rating = []
        self.matches__a_rating = []
        self.matches__error = []

        self.match_processed_count = 0
        self.MSE = 0

    def process_matches(self, h_team_ids, a_team_ids, h_team_scores, a_team_scores):
        """
        Gradually updates pirating of teams, based on sequence of matches.

        Each team starts with home and away rating equal to 0.
        After each match, ratings are adjusted and stored to be used when the team plays again.

        Arguments should be lists of the same length. N-th match is defined using n-th elements of the lists.
        Matches (= elements of the lists) need to be ordered from the oldest to the newest.
        :param h_team_ids: list; for each match, ID of the home team
        :param a_team_ids: list; for each match, ID of the away team
        :param h_team_scores: list; for each match, score of the home team
        :param a_team_scores: list; for each match, score of the away team
        """
        for (h_id, a_id, h_score, a_score) in zip(h_team_ids, a_team_ids, h_team_scores, a_team_scores):
            self.match_processed_count += 1
            new_ratings_and_error = self.compute_pi_ratings(self.teams__h_rating.get(h_id, 0),
                                                            self.teams__a_rating.get(h_id, 0),
                                                            self.teams__h_rating.get(a_id, 0),
                                                            self.teams__a_rating.get(a_id, 0), h_score, a_score)
            if self.store_history:
                self.matches__h_rating.append(self.teams__h_rating.get(h_id, 0))
                self.matches__a_rating.append(self.teams__a_rating.get(a_id, 0))
                self.matches__error.append(new_ratings_and_error[4])

            self.MSE = ((self.match_processed_count - 1) * self.MSE + new_ratings_and_error[4]) / self.match_processed_count

            self.teams__h_rating[h_id] = new_ratings_and_error[0]
            self.teams__a_rating[h_id] = new_ratings_and_error[1]
            self.teams__h_rating[a_id] = new_ratings_and_error[2]
            self.teams__a_rating[a_id] = new_ratings_and_error[3]

    def compute_pi_ratings(self,
                           h_team__h_rating, h_team__a_rating,
                           a_team__h_rating, a_team__a_rating,
                           h_team_scored, a_team__scored) -> (float, float, float, float, float):
        """
        Returns new piratings of the two teams and error of the score prediction.
        Computation is based on the teams old piratings and their scores in the match.

        :param h_team__h_rating: current "home rating" of the home team
        :param h_team__a_rating: current "away rating" of the home team
        :param a_team__h_rating: current "home rating" of the away team
        :param a_team__a_rating: current "away rating" of the away team
        :param h_team_scored: score of the home team in the match
        :param a_team__scored: score of the away team in the match
        :return: list containing elements in the following order
            new "home rating" of the home team,
            new "away rating" of the home team,
            new "home rating" of the away team,
            new "away rating" of the away team,
            squared error of the score prediction
        """
        exp_home_score_diff = np.sign(h_team__h_rating) * (10 ** (np.abs(h_team__h_rating) / self.c) - 1)
        exp_away_score_diff = np.sign(a_team__a_rating) * (10 ** (np.abs(a_team__a_rating) / self.c) - 1)

        exp_score_diff = exp_home_score_diff - exp_away_score_diff
        actual_score_diff = h_team_scored - a_team__scored
        e = actual_score_diff - exp_score_diff

        psi_h = np.sign(e) * self.c * np.log10(1 + np.abs(e))
        psi_a = -psi_h

        new_h_team__h_rating = h_team__h_rating + psi_h * self.lamb
        new_h_team__a_rating = h_team__a_rating + psi_h * self.lamb * self.gamma
        new_a_team__h_rating = a_team__h_rating + psi_a * self.lamb * self.gamma
        new_a_team__a_rating = a_team__a_rating + psi_a * self.lamb

        return new_h_team__h_rating, new_h_team__a_rating, new_a_team__h_rating, new_a_team__a_rating, e**2

    def predict_score_diff(self, h_team__h_rating, a_team__a_rating):
        """
        Predict score difference between two teams based on their piratings.

        :param h_team__h_rating: current "home rating" of the home team
        :param a_team__a_rating: current "away rating" of the away team
        :return: Predicted score difference
        """
        exp_home_score_diff = np.sign(h_team__h_rating) * (10 ** (np.abs(h_team__h_rating) / self.c) - 1)
        exp_away_score_diff = np.sign(a_team__a_rating) * (10 ** (np.abs(a_team__a_rating) / self.c) - 1)
        return exp_home_score_diff - exp_away_score_diff


class Optimization:
    """
        Class for optimization of the pirating model parameters.
        Performs grid search using values prescribed in the arguments.
        Grid search tries each combination of the values specified in the arguments
        and returns combination for which the model has lowest error.

        The most important are parameters are learning rates "gamma" and "lambda".
        Models with constant "c" set to default value 3 usually perform well.

        Parameters
            lamb_grid : list
                learning rate for rating updates, describing how fast rating reacts on recent performance
            gamma_grid : list
                additional learning rate, describing how fast "home rating" reacts on away performance and vice-versa
            c_grid : list
                constant, default value 3
        """
    def __init__(self, c_grid=None, gamma_grid=None, lamb_grid=None):
        """
        Sets grid for values "c", "lambda", "gamma". If grid=None - uses default value.
        """
        self.c_grid = c_grid if (c_grid is not None) else [3]
        self.gamma_grid = gamma_grid if (gamma_grid is not None) else [0.64 + 0.02*x for x in range(0, 8, 1)]
        self.lamb_grid = lamb_grid if (lamb_grid is not None) else [0.033 + 0.001*x for x in range(0, 8, 1)]

        self.grids_ok = True if ((self.c_grid and self.gamma_grid and self.lamb_grid) is not False) else False

    def grid_search(self, h_team_ids, a_team_ids, h_team_scores, a_team_scores):
        """
        For data (matches) given in the arguments perform grid search.

        Func tries each combination of the values of "lambda", "gamma" and "c" specified in class attributes.
        For each combination, it trains PiRatingsModel on the data given in this function arguments.
        Then returns the combination with the lowest prediction error.

        Arguments should be lists of the same length. N-th match is defined using n-th elements of the lists.
        Matches (= elements of the lists) need to be ordered from the oldest to the newest.
        :param h_team_ids: list; for each match, ID of the home team
        :param a_team_ids: list; for each match, ID of the away team
        :param h_team_scores: list; for each match, score of the home team
        :param a_team_scores: list; for each match, score of the away team
        :return: dict; Values of parameters "lambda", "gamma", "c" for which the model had lowest prediction error.
        """
        if not self.grids_ok:
            print("Cannot perform grid search, missing parameter values."
                  "One of the lists of possible values of c, gamma or lambda is empty.")

        best_params = {}
        best_error = float("inf")
        for c in self.c_grid:
            for gamma in self.gamma_grid:
                for lamb in self.lamb_grid:
                    print("Another")
                    model = PiRatingsModel(c, gamma, lamb, False)
                    model.process_matches(h_team_ids, a_team_ids, h_team_scores, a_team_scores)
                    if model.MSE < best_error:
                        best_error = model.MSE
                        best_params['c'] = c
                        best_params['gamma'] = gamma
                        best_params['lambda'] = lamb
                    print('c {}, gamma {}, lambda {}, error {}'.format(c, gamma, lamb, model.MSE))

        return best_params
